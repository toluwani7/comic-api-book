const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled UglifyJSPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/uglifyjs-webpack-plugin
 *
 */

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  mode: 'production',
  cache: true,
  entry: './src/index.js',
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'css/style.css',
      chunkFilename: 'css/style[id].css'
    }),
    new CopyPlugin([
      {
        from: './src/locales',
        to: 'locales',
        fromType: 'dir'
      }
    ]),
    new webpack.optimize.AggressiveSplittingPlugin({
      minSize: 30000,
      maxSize: 50000
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })
  ],
  recordsOutputPath: path.join(__dirname, 'dist', 'records.json'),
  module: {
    rules: [
      {
        include: [path.resolve(__dirname, 'src')],
        loader: 'babel-loader',

        options: {
          plugins: ['syntax-dynamic-import'],

          presets: [
            [
              '@babel/preset-env',
              {
                modules: false,
                targets: { electron: '1.6.0' }
              }
            ]
          ]
        },
        test: /\.js$/
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              publicPath: '../',
              hmr: process.env.NODE_ENV === 'development'
            }
          },
          'css-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'static/media/[name].[ext]'
            }
          }
        ]
      }
      /*       {
        type: 'javascript/auto',
        test: /\.(json|html)/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'static/json/[name].[ext]' },
          },
        ],
      }, */
      // Font files
      // {
      //   test: /\.(woff|woff2|ttf|otf|eot)$/,
      //   loader: 'file-loader',

      //   options: {
      //     name: 'media/[name].[ext]',
      //   },
      // },
    ]
  },

  output: {
    chunkFilename: 'js/[name].js',
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'dist')
  },

  optimization: {
    minimizer: [
      new TerserPlugin({
        test: /\.js(\?.*)?$/i
      }),
      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.optimize\.css$/g
      })
    ],

    splitChunks: {
      chunks: 'all'
    }
  }
};
