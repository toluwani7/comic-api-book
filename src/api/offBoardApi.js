import axios from 'axios';
export const fetchData = url => {
  return axios
    .get(url)
    .then(response => {
      // Success
      return Promise.resolve(response);
    })
    .catch(error => {
      // Error
      return Promise.reject(error);
    });
};
