import { FETCH_COMICS } from './actionTypes';
import { fetchData } from '../api/offBoardApi';
import store from '../store/index';

//reset
export const resetComics = () => {
  store.dispatch({
    type: FETCH_COMICS,
    payload: {}
  });
};
// actions
export const fetchComics = (
  id = null,
  url = 'https://cors-anywhere.herokuapp.com/http://xkcd.com/info.0.json'
) => {
  if (id !== null) {
    url = `https://cors-anywhere.herokuapp.com/https://xkcd.com/${id}/info.0.json`;
  }
  return fetchData(url)
    .then(response => response.data)
    .then(
      comics => {
        console.log('data', comics);
        store.dispatch({
          type: FETCH_COMICS,
          payload: comics
        });
      },
      error => {
        // TODO: create error handling
        store.dispatch({
          type: FETCH_COMICS,
          payload: {}
        });
        console.log('There has been an error');
      }
    );
};
