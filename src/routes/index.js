import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { MemoryRouter as Router } from 'react-router-dom';
import HomeScreen from '../screens/HomeScreen';
import SearchComicScreen from '../screens/SearchComicScreen';

const Index = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={HomeScreen} />
      <Route exact path="/search" component={SearchComicScreen} />
    </Switch>
  </Router>
);

export default Index;
