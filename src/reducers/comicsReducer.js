import { FETCH_COMICS } from '../actions/actionTypes';

const initialState = {
  data: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_COMICS:
      return {
        ...state,
        data: action.payload
      };
    default:
      return state;
  }
}
