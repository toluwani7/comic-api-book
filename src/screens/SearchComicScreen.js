import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SearchBox from '../components/SearchBox';
import Navigation from '../components/Navigation';

class SearchComicScreen extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.state = {
      isHovering: false
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering
    };
  }

  componentDidMount() {}

  render() {
    const { comics } = this.props;
    const hasComic = Object.keys(comics).length > 0;
    const redirectURL = 'https://xkcd.com/' + comics.num;
    console.log('comic data from store', this.props.comics);
    return (
      <div>
        <Navigation />
        <SearchBox />
        {hasComic ? (
          <div>
            <p className={'test'}>{comics.title}</p>
            <p className={'test'}>{comics.num}</p>
            {this.state.isHovering && <div>{comics.alt}</div>}
            <a href={redirectURL} target={'_blank'}>
              <img
                onMouseEnter={this.handleMouseHover}
                onMouseLeave={this.handleMouseHover}
                src={comics.img}
                alt={comics.alt}
              ></img>
            </a>
          </div>
        ) : (
          <div>
            <p className={'test'}>Enter a valid comic ID</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  comics: state.comics.data
});

export default connect(mapStateToProps)(SearchComicScreen);
