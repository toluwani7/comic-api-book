import React from 'react';
import { Link } from 'react-router-dom';
import { resetComics } from '../actions/fetchActions';
import '../components/navigation.styles.css';

class Navigation extends React.Component {
  resetComics() {}
  render() {
    return (
      <div>
        <ul className="list-group">
          <li className="list-group-item">
            <Link to={'/'}>Recent Comics</Link>
          </li>
          <li className="list-group-item">
            <Link to={'/search'} onClick={resetComics}>
              Search Comics
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default Navigation;
