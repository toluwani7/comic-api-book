import React from 'react';
import PropTypes from 'prop-types';
import store from '../store';
import '../components/search.styles.css';
import { fetchComics } from '../actions/fetchActions';

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.getComicsById = this.getComicsById.bind(this);
  }

  getComicsById() {
    const searchInput = document.querySelector('[type="number"]');
    console.log('logo', searchInput.value);
    if (searchInput.value) {
      return fetchComics(searchInput.value);
    }
  }

  componentDidMount() {}

  render() {
    return (
      <input
        className="search"
        type="number"
        placeholder={this.props.placeholder}
        onChange={this.getComicsById}
      />
    );
  }
}

export default SearchBox;
